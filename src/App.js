import React from "react";
import "./Styles/App.scss";
import Characters from "./components/Characters";

function App() {
	return (
		<div className="App">
			<Characters />
		</div>
	);
}

export default App;
